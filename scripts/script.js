var weatherApp = angular.module("weatherApp", ['ngAria']);

weatherApp.controller('WeatherCtrl', function($scope, $http) {

    $http.get('http://api.openweathermap.org/data/2.5/forecast/daily?id=7291968&cnt=5&APPID=75835bfa9638957c80f155d9baba9ccb').success(function(data) {
        $scope.weatherReport = data;
        $scope.weatherByDay = data.list;
        $scope.tab = data.list[0].dt;
    });


    $scope.setTab = function setTab(tabId) {
        $scope.tab = tabId;
    }
});

weatherApp.filter("kelvinToCelsius", function() {
    return function(kelvin) {
        return parseFloat((kelvin) - 273.15).toFixed(0);
    };
});
